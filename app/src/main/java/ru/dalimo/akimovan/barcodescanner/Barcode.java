package ru.dalimo.akimovan.barcodescanner;

/**
 * Created by AkimovAN on 29.12.2017.
 */

public class Barcode {
    private String product_code;
    private String weight;
    private boolean delChek;

    public Barcode(String product_code, String weight) {
        this.product_code = product_code;
        this.weight = weight;
        delChek = false;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public boolean isDelChek() {
        return delChek;
    }

    public void setDelChek(boolean delChek) {
        this.delChek = delChek;
    }
}
