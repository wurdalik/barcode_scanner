package ru.dalimo.akimovan.barcodescanner;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.InputType;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class ScannerFragment extends ListFragment {
    private static final String TAG = "ScannerFragment";
    private final String APP_PREFERENCES = "mysettings";
    private final String[] DEFAULT_VALUE_PREFIX = {"21", "22"};
    private EditText mEditText;
    private ListView mListBarcodes;
    private TextView totalWeight;
    private Button resetBtn;
    private Button lastDelBtn;
    private ListView listPref;
    private Barcode mBarcode;
    private float mass;
    private float finalMass;
    private String barCode;
    private String barPref;
    private String mesCode;
    private String mesWeight;
    private CodeAdapter mCodeAdapter;
    private PrefixAdapter adapter;
    List<Barcode> codesArray = new ArrayList<>();
    List<String> codePrefix;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        codePrefix = load(APP_PREFERENCES, DEFAULT_VALUE_PREFIX);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.new_fragment_view, null);
        resetBtn = v.findViewById(R.id.reset);
        lastDelBtn = v.findViewById(R.id.delete_last);
        mEditText = v.findViewById(R.id.barcode_edit);
        totalWeight = v.findViewById(R.id.final_mass);
        mListBarcodes = v.findViewById(android.R.id.list);
        mListBarcodes.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        mListBarcodes.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, final int position, long id, boolean checked) {
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                SparseBooleanArray checked = mListBarcodes.getCheckedItemPositions();
                inflater.inflate(R.menu.delete_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                delBarcode(codesArray, finalMass);
                mode.finish();
                reuseAdapter();
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }

            private List<Integer> getSelectedFiles() {
                List<Integer> selected = new ArrayList<Integer>();

                SparseBooleanArray sparseBooleanArray = mListBarcodes.getCheckedItemPositions();
                for (int i = 0; i < sparseBooleanArray.size(); i++) {
                    if (sparseBooleanArray.valueAt(i)) {
                        int selectedItem = (int) mListBarcodes.getItemIdAtPosition(sparseBooleanArray.keyAt(i));
                        selected.add(selectedItem);
                    }
                }
                return selected;
            }

            public void delBarcode(List<Barcode> barcode, float finalMass) {
                List<Integer> selected = getSelectedFiles();
                Collections.reverse(selected);
                for (int i = 0; i <= selected.size() - 1; i++) {
                    for (int j = barcode.size() - 1; j >= 0; j--) {
                        if (j == selected.get(i)) {
                            finalMass -= parceWeight(barcode.get(j).getWeight());
                            barcode.remove(j);
                        }
                    }
                }
                calcMass(finalMass, codesArray, totalWeight);
                reuseAdapter();
            }

        });

        mEditText.setInputType(InputType.TYPE_CLASS_NUMBER);

        mEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                    mEditText.setText(String.format("%s%s", mEditText.getText(), "\n"));
                    String data = mEditText.getText().toString();
                    if (!data.equals("\n") && !data.equals("")) {
                        prepareBarcode(data);
                    } else {
                        mEditText.getText().clear();
                    }
                    return true;
                }

                return false;
            }
        });

//кнопка сброс
        resetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codesArray.clear();
                reuseAdapter();
                calcMass(finalMass, codesArray, totalWeight);
            }
        });


//кнопка удалить последний
        lastDelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (codesArray.size() == 0) {
                } else {
                    int i = codesArray.size() - 1;
                    codesArray.remove(i);
                    calcMass(finalMass, codesArray, totalWeight);
                    mCodeAdapter.notifyDataSetChanged();
                }

            }

        });

        reuseAdapter();
        return v;
    }

    //деление штрихкода на код/массу и добавление в адаптер
    private void prepareBarcode(String data) {
        data.replace("\n", "");
        int i = data.length();
        if (i >= 12) {
            String bc = "";
            if (i >= 12) {
                bc = data.substring(0, i);
                barPref = bc.substring(0, 2);
                barCode = bc.substring(2, 7);
                mass = Float.valueOf(bc.substring(7, bc.length())) / 1000;
                if (load(APP_PREFERENCES, DEFAULT_VALUE_PREFIX).contains(barPref)) {
                    mesCode = String.format("Код товара: %s", barCode);
                    mesWeight = String.format("%.3f", mass);
                    mBarcode = new Barcode(mesCode, mesWeight);
                    codesArray.add(mBarcode);
                } else {
                    mesCode = new String("Неверный префикс: " + barPref);
                    mesWeight = new String("");
                    mBarcode = new Barcode(mesCode, mesWeight);
                    codesArray.add(mBarcode);
                }
                reuseAdapter();
            }
            mListBarcodes.setSelection(mListBarcodes.getCount() - 1);
            mEditText.getText().clear();
            calcMass(finalMass, codesArray, totalWeight);
        } else {
            makeToast(getContext(), "Некорректный штрихкод");
            mEditText.getText().clear();
        }

    }

    //формирование массива для адаптера префиксов (для спиннера в настройках)
    public String[] prefArray() {
        String prefix[] = new String[100];
        for (int i = 0; i <= prefix.length - 1; i++) {
            if (i >= 0 && i <= 9) {
                prefix[i] = String.format("0%s", String.valueOf(i));
            } else {
                prefix[i] = String.valueOf(i);
            }
        }
        return prefix;
    }

    //парсинг веса
    private float parceWeight(String weight) {
        float mWeight;
        String clear;
        if (!weight.equalsIgnoreCase("")) {
            clear = weight.substring(0, weight.length()).replace(",", "");
            mWeight = Float.parseFloat(clear) / 1000;
        } else {
            mWeight = 0;
        }
        return mWeight;
    }

    //установка адаптера
    private void reuseAdapter() {
        mCodeAdapter = new CodeAdapter(getActivity(), R.id.list_item_code, codesArray, false);
        mListBarcodes.setAdapter(mCodeAdapter);
        mCodeAdapter.notifyDataSetChanged();
    }

    //формирование Toasty
    public static void makeToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }


    //подсчет общей массы
    private void calcMass(float mass, List<Barcode> barcodes, TextView finalMass) {
        for (int i = 0; i <= barcodes.size() - 1; i++) {
            mass += parceWeight(barcodes.get(i).getWeight());
        }
        finalMass.setText(String.format("%.3f", mass));
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        if (!writeToFile(codesArray, filename)){
//            Toast.makeText(getActivity(), "Не удалось записать файл " + filename, Toast.LENGTH_SHORT);
//        }
    }

    //открытие диалога настроек
    private void openSettings() {
        AlertDialog.Builder settings = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        String prefixAr[] = prefArray();
        View v = inflater.inflate(R.layout.list_prefix, null);
        Button addPref = v.findViewById(R.id.add_new_prefix);

        final AutoCompleteTextView newPref = v.findViewById(R.id.new_prefix);
        newPref.setInputType(InputType.TYPE_CLASS_NUMBER);

        final ArrayAdapter<String> prefSpin = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, prefixAr);
        newPref.setAdapter(prefSpin);

        final List<String> prefix = load(APP_PREFERENCES, DEFAULT_VALUE_PREFIX);
        Collections.sort(prefix);

        adapter = new PrefixAdapter(this.getContext(), R.id.list_item_prefix, prefix);

        listPref = v.findViewById(android.R.id.list);

        listPref.setAdapter(adapter);

        settings.setTitle("Список префиксов:");
        settings.setView(v);
        final AlertDialog getSettings = settings.create();

        Button cancel = v.findViewById(R.id.cancel_action);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSettings.dismiss();
            }
        });
        newPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPref.showDropDown();
            }
        });


        addPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPref.getText().toString().equals("")) {
                    makeToast(getSettings.getContext(), "Введите префикс");

                } else {

                    if (!load(APP_PREFERENCES, DEFAULT_VALUE_PREFIX).contains(newPref.getText().toString())) {
                        save(APP_PREFERENCES, newPref.getText().toString(), DEFAULT_VALUE_PREFIX);
                        makeToast(getSettings.getContext(), "Префикс " + newPref.getText().toString() + " добавлен");
                        newPref.getText().clear();
                        List<String> sort = load(APP_PREFERENCES, DEFAULT_VALUE_PREFIX);
                        Collections.sort(sort);
                        adapter = new PrefixAdapter(getActivity(), R.id.list_item_prefix, sort);
                        listPref.setAdapter(adapter);
                        listPref.setSelection(listPref.getCount() - 1);
                        adapter.notifyDataSetChanged();
                    } else {
                        makeToast(getSettings.getContext(), "Префикс уже используется");
                    }
                }
            }
        });

        getSettings.show();

    }



    /**
     * На запись штрихкода в текстовый файл
     * <p>
     * private boolean writeToFile(List<String> list, String filename) {
     * Set<String> hs = new HashSet<>();
     * hs.addAll(list);
     * list.clear();
     * list.addAll(hs);
     * try {
     * OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getActivity().openFileOutput(filename, Context.MODE_PRIVATE));
     * outputStreamWriter.write(list.toString());
     * outputStreamWriter.close();
     * return true;
     * } catch (IOException e) {
     * return false;
     * }
     * }
     */

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.delete_menu, menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.settings_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                openSettings();
                codePrefix = load(APP_PREFERENCES, DEFAULT_VALUE_PREFIX);
                break;

            case R.id.language:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MAIN);
                ComponentName com = new ComponentName("com.android.settings", "com.android.settings.LanguageSettings");
                intent.setComponent(com);
                startActivity(intent);
                break;
        }
        return true;
    }


    //сохранение выбранного префикса
    private void save(String settingsName, String pref, String[] defaultValues) {
        SharedPreferences mSettings = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = mSettings.edit();
        Set<String> defaultPref = new HashSet<>(Arrays.asList(defaultValues));
        Set<String> savePref = mSettings.getStringSet(settingsName, defaultPref);
        Set<String> prefix = new HashSet<>();
        prefix.addAll(savePref);
        prefix.add(pref);
        ed.putStringSet(settingsName, prefix);
        ed.commit();
    }

    //загрузка сохраненных префиксов из SharedPreferences
    private List<String> load(String settingsName, String[] defValues) {
        Set<String> values = new HashSet<>(Arrays.asList(defValues));
        List<String> prefix = new ArrayList<>();
        SharedPreferences mSettings = getActivity().getPreferences(Context.MODE_PRIVATE);
        Set<String> pref = mSettings.getStringSet(settingsName, values);
        prefix.addAll(pref);
        return prefix;
    }

    //удаление префикса
    private void delPrefix(String prefixCode, String settingsName, String[] defaultValues, PrefixAdapter adapterName, ListView listName) {
        SharedPreferences mSettings = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = mSettings.edit();
        Set<String> defaultPref = new HashSet<>(Arrays.asList(defaultValues));
        Set<String> savePref = mSettings.getStringSet(settingsName, defaultPref);
        Set<String> prefix = new HashSet<>();
        prefix.addAll(savePref);
        prefix.remove(prefixCode);
        ed.putStringSet(settingsName, prefix);
        ed.commit();
        List<String> prefSort = load(settingsName, defaultValues);
        Collections.sort(prefSort);
        adapterName = new PrefixAdapter(getActivity(), R.id.list_item_prefix, prefSort);
        listName.setAdapter(adapterName);
        adapterName.notifyDataSetChanged();
    }

    public class CodeAdapter extends ArrayAdapter<Barcode> {
        private List<Barcode> codeArray;
        boolean delMode;
        LayoutInflater inflater;
        private int layout;

        public CodeAdapter(Context context, int resource, List<Barcode> code, boolean delMode) {
            super(context, resource, code);
            codeArray = code;
            this.layout = resource;
            this.inflater = LayoutInflater.from(context);
            this.delMode = delMode;
        }

        @Override
        public int getCount() {
            return codeArray.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            View view = convertView;
            if (convertView == null) {
                view = inflater.inflate(R.layout.list_item_code, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.mCodeProd = view.findViewById(R.id.list_item_code_code);
                viewHolder.mCodeWeight = view.findViewById(R.id.list_item_code_mass);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            Barcode mBarcode = codeArray.get(position);

            viewHolder.mCodeProd.setText(String.format((position + 1) + ". %s", mBarcode.getProduct_code()));
            viewHolder.mCodeWeight.setText(String.format("%s", mBarcode.getWeight()));

            return view;
        }

        private class ViewHolder {
            TextView mCodeProd;
            TextView mCodeWeight;
        }
    }

    public class PrefixAdapter extends ArrayAdapter<String> {
        LayoutInflater inflater;
        private List<String> prefixCode;
        private int layout;


        public PrefixAdapter(Context context, int resource, List<String> prefix) {
            super(context, resource, prefix);
            prefixCode = prefix;
            this.layout = resource;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            if (prefixCode == null) {
                return 0;
            } else {
                return prefixCode.size();
            }
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            View view = convertView;
            if (convertView == null) {
                view = inflater.inflate(R.layout.list_item_prefix, parent, false);
                viewHolder = new PrefixAdapter.ViewHolder();
                viewHolder.mPref = view.findViewById(R.id.prefix);
                viewHolder.delPref = view.findViewById(R.id.del_pref);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            String prefix = String.valueOf(prefixCode.get(position));
            viewHolder.mPref.setText(prefix);

            viewHolder.delPref.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delPrefix(getItem(position), APP_PREFERENCES, DEFAULT_VALUE_PREFIX, adapter, listPref);
                }
            });

            return view;
        }

        private class ViewHolder {
            TextView mPref;
            ImageButton delPref;
        }
    }


}
